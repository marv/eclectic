" Vim syntax file
" Language:     eclectic module
" Author:       Saleem Abdulrasool <compnerd@compnerd.org>
" Copyright:    Copyright © 2012 Saleem Abdulrasool <compnerd@compnerd.org>
" Version:      $Revision$

" Determine if we need to execute {{{
if &compatible || v:version < 603
    finish
endif

if exists("b:current_syntax")
    finish
endif
" }}}

let is_bash=1
runtime! syntax/sh.vim
unlet b:current_syntax

" Cave Keywords: {{{
syn keyword eclecticBannedKeyword cave_common cave_envvar
syn match eclecticBannedKeyword /cave_has-version/
" }}}

" Core Keywords: {{{
syn keyword eclecticCoreKeyword check_do die do_action inherit eval sed
" }}}

" Config Keywords: {{{
syn keyword eclecticConfKeyword store_config load_config add_config
" }}}

" Manip Keywords: {{{
syn keyword eclecticManipKeyword svn_date_to_version
" }}}

" Multilib Keywords: {{{
syn keyword eclecticMultilibKeyword list_libdirs
" }}}

" Output Keywords: {{{
syn keyword eclecticOutputKeyword write_error_msg write_warning_msg
syn keyword eclecticOutputKeyword write_list_start
syn keyword eclecticOutputKeyword write_kv_list_entry
syn keyword eclecticOutputKeyword write_numbered_list_entry write_numbered_list
syn keyword eclecticOutputKeyword get_column_width
syn keyword eclecticOutputKeyword apply_text_highlight highlight highlight_warning
syn keyword eclecticOutputKeyword space
" }}}

" Path Manipulation Keywords: {{{
syn keyword eclecticPathKeyword basename dirname
" }}}

" Package Manager Keywords: {{{
syn keyword eclecticPMKeyword arch envvar
syn match eclecticPMKeyword /has-version/
syn keyword eclecticPMKeyword get_repositories get_news_dir_name
syn keyword eclecticPMKeyword get_config_protect_dirs
syn match eclecticPMKeyword /package-manager/
" }}}

" Portage Keywords: {{{
syn keyword eclecticBannedKeyword portageq
syn keyword eclecticBannedKeyword portage_get_repo_name portage_get_repository
" }}}

" Test Keywords: {{{
syn keyword eclecticTestKeyword has is_function is_number
" }}}

" Metadata Variables: {{{
syn keyword eclecticMetadataVariable AUTHOR DESCRIPTION MAINTAINER VERSION
" }}}

" Implementation: {{{
syn region eclecticFunction start="^\(describe\|do\)_[-a-zA-Z0-9_]*()\_s*{" end="}" contains=shFunctionKey,@shFunctionList skipwhite skipnl nextgroup=shFunctionStart,shQuickComment
" }}}

" Clusters: {{{
syn cluster eclecticKeywords contains=eclecticConfKeyword,eclecticCoreKeyword
syn cluster eclecticKeywords add=eclecticManipKeyword,eclecticMultilibKeyword
syn cluster eclecticKeywords add=eclecticOutputKeyword,eclecticPathKeyword
syn cluster eclecticKeywords add=eclecticPMKeyword,eclecticTestKeyword

syn cluster eclecticVariables contains=eclecticMetadataVariable

syn cluster eclecticBanned contains=eclecticBannedKeyword

syn cluster shCommandSubList add=@eclecticKeywords,@eclecticBanned
" }}}

" Default Highlighting: {{{
hi def link eclecticConfKeyword Function
hi def link eclecticCoreKeyword Function
hi def link eclecticManipKeyword Function
hi def link eclecticMultilibKeyword Function
hi def link eclecticOutputKeyword Function
hi def link eclecticPathKeyword Function
hi def link eclecticPMKeyword Function
hi def link eclecticTestKeyword Function

hi def link eclecticMetadataVariable Identifier

hi def link eclecticBannedKeyword Error

hi def link eclecticFunction PreProc
" }}}

" Set Current Syntax: {{{
let b:current_syntax = "eclectic"
" }}}

" vim: set et fdm=marker fmr={{{,}}} ft=vim sts=4 sw=4 ts=8:

