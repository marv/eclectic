eclectic Release Guide
======================

.. Note:: This guide is intended for people who do ``eclectic`` releases. It is
  probably of no interest to the rest of you.

Update version in configure.ac.

Make sure ChangeLog is tidy.  Add ChangeLog entry for updating configure.in
and tagging the release.

Update NEWS file with a summary of changes mentioned in ChangeLog since the
last release.

Do a test run in trunk/. ::

    $ ./autogen.bash
    $ ./configure
    $ make dist
    $ mv ${P}.tar.bz2 ${DISTDIR}
    
Test with an ebuild and make sure everything builds and everything that
should be included in the tarball is.

Test eclectic itself and as many modules as you can.

Commit any changes you may have made in the previous steps.

Tag release. ::

    $ make maintainer-clean
    $ svn copy trunk tags/release-${PV}
    $ svn commit -m "Tagged ${PV} release."

Build the final tarball. ::

    $ cd tags/release-${PV}
    $ ./autogen.sh
    $ ./configure
    $ make dist-bzip2

Sign it (there may be a dist-sign target in the future). ::

    $ gpg --armor --detach-sign ${P}.tar.bz2

.. vim: set ft=glep tw=80 sw=4 et spell spelllang=en : ..
