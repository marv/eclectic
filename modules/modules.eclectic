# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

DESCRIPTION="A module for querying modules. By default, it lists all available modules"
MAINTAINER="eclectic@exherbo.org"
SVN_DATE='$Date: $'
VERSION=$(svn_date_to_version "${SVN_DATE}" )

DEFAULT_ACTION="list"

inherit config output tests

### list action

## {{{ list

describe_list() {
    echo "Lists all available modules."
}

# List all installed modules
do_list() {
    local path file module name desc group groups Extra_modules

    write_list_start "Built-in modules:"
    write_kv_list_entry "help"          "Display a help message"
    write_kv_list_entry "usage"         "Display a usage message"
    write_kv_list_entry "version"       "Display version information"
    write_kv_list_entry "print-modules" "Print eclectic modules"
    write_kv_list_entry "print-actions" "Print actions for a given module"
    write_kv_list_entry "print-options" "Print options for a given action"

    for path in "${ECLECTIC_MODULES_PATH[@]}" ; do
        [[ -d ${path} ]] || continue
        for file in ${path}/*.eclectic ; do
            [[ -f ${file} ]] || continue
            group=$(load_config "${file}" ECLECTIC_MODULE_GROUP)
            if [[ -n ${group} ]]; then
                has ${group} ${groups} || groups+=" ${group}"
                declare ${group}_modules+=" ${file}"
            else
                Extra_modules+=" ${file}"
            fi
        done
    done

    for group in ${groups} Extra; do
        local m
        m="${group}_modules"
        if [[ -n ${!m} ]] ; then
            echo
            write_list_start "${group} modules:"
            for module in ${!m}; do
                name=${module##*/}
                name=${name%%.eclectic}
                desc=$(load_config "${module}" DESCRIPTION)
                desc=${desc:-No description available}
                write_kv_list_entry "${name}" "${desc}"
            done
        fi
    done
}

# }}}

### group action

## {{{ group

describe_group() {
    echo "Lists all available modules belonging to a specified group."
}

describe_group_parameters() {
    echo "<group>"
}

do_group() {
    local path file groupname="$1" group module modules name desc
    [[ -z "$groupname" ]] && die -q "Required option (group name) missing."

    if [[ ${groupname} == Built-in ]]; then
        write_list_start "Built-in modules:"
        write_kv_list_entry "help"          "Display a help message"
        write_kv_list_entry "usage"         "Display a usage message"
        write_kv_list_entry "version"       "Display version information"
        return 0
    fi

    for path in "${ECLECTIC_MODULES_PATH[@]}" ; do
        [[ -d ${path} ]] || continue
        for file in ${path}/*.eclectic ; do
            [[ -f ${file} ]] || continue
            group=$(load_config "${file}" ECLECTIC_MODULE_GROUP)
            [[ ${groupname} == ${group} ||
                ( ${groupname} == Extra && -z ${group} ) ]] || continue
            modules+=" ${file}"
        done
    done

    if [[ -n ${modules} ]] ; then
        write_list_start "${groupname} modules:"
        for module in ${modules}; do
            name=${module##*/}
            name=${name%%.eclectic}
            desc=$(load_config "${module}" DESCRIPTION)
            desc=${desc:-No description available}
            write_kv_list_entry "${name}" "${desc}"
        done
    else
        die -q "No modules belonging to ${groupname} was found"
    fi
}

# }}}

### has action

## {{{ has 
describe_has() {
    echo "Returns true if the module is installed, and false otherwise."
}

describe_has_parameters() {
    echo "<module>"
}

do_has() {
    local modname="$1" modpath
    [[ -z "$modname" ]] && die -q "Required option (module name) missing."
    for modpath in "${ECLECTIC_MODULES_PATH[@]}" ; do
        [[ -f "${modpath}/${modname}.eclectic" ]] && return 0
    done
    return 1
}
## }}}

### add action

## {{{ add

describe_add() {
    echo "Installs a module file to \$HOME/.eclectic/modules/, or ${ECLECTIC_DEFAULT_MODULES_PATH//\/\///}/ when run as root"
}

describe_add_parameters() {
    echo "<module_file>"
}

do_add() {
    local local_path="${ROOT}${HOME}/.eclectic/modules/" module_file
    local force_default=0

    if [[ ${1} == "--force-default-location" ]] ; then
        force_default=1
        shift
    fi
    module_file=${1}

    [[ -z "${module_file}" ]] && die -q "Required option (module file) missing."

    if ! cp "${module_file}" "${ECLECTIC_DEFAULT_MODULES_PATH}" &> /dev/null ; then
        [[ ${force_default} == 1 ]] \
            && die -q "Failed to install module file to default modules path."

        mkdir -p "${local_path}" \
            || die -q "Failed to create module install directory."
        cp "${module_file}" "${local_path}" \
            || die -q "Failed to install module file."
    fi
}

## }}}

# vim: set ft=eclectic sw=4 sts=4 ts=4 et tw=80 :
