#!/usr/bin/env bash

# Copyright (c) 2005, 2007 Gentoo Foundation.
# This file is part of the 'eclectic' tools framework.
#
# eclectic is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# eclectic is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eclectic; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA

inherit cave portage

# arch
# Return the architecture we're running on...
arch() {
    local ret=$(envvar sys-devel/gcc ARCH) suffix

    # $arch will be null if there's no current make.profile symlink
    # we cannot get a list of valid profiles without it.
    if [[ -z ${ret} ]] ; then

        if [[ -n "${ROOT}" && ${ROOT} != "/" ]] ; then
            write_warning_msg "Failed to determine \${ARCH}. Is your make.profile symlink valid?"
            return 1
        fi

        ret=$(uname -m)
        case ${ret} in
            alpha|ia64|ppc) ;;
            i?86) ret=x86 ;;
            mips*) ret=mips ;;
            sparc*) ret=sparc ;;
            x86_64) ret=amd64 ;;
            *) write_warning_msg \
                "Unknown architecture. Please submit a bug including the output of 'uname -m'!"
                return 1
                ;;
            esac

            case $(uname -s) in
                Linux) ;;
                FreeBSD) suffix="-fbsd" ;;
                NetBSD) suffix="-nbsd" ;;
                OpenBSD) suffix="-obsd" ;;
                DragonFly) suffix="-dfly" ;;
                *) write_warning_msg \
                    "Unknown OS. Please submit a bug including the output of 'uname -s'!"
                    return 1
                    ;;
            esac
    fi

    echo ${ret}${suffix}
}

# envvar
# Return the contents of environment variable $2 as seen by package manager(s)
# for package $1.
envvar() {
    [[ ${#@} -eq 2 ]] || die "envvar expects exactly 2 arguments!"

    local manager=$(package-manager)
    case ${manager} in
        cave)
            cave_envvar $* 2>/dev/null
            ;;
        portage)
            # portage does not support per-package envvar lookup.
            portageq envvar "${2}" 2>/dev/null
            ;;
        *)
            die "Unknown package manager: \"${manager}\"!"
    esac
}

# has-version
# Return true if package $2 is available in ${ROOT}
has-version() {
    [[ ${#@} -eq 1 ]] || die "has_version expects exactly 1 arguments!"

    local manager=$(package-manager)
    case ${manager} in
        cave)
            cave_has-version "${1}" 2>/dev/null
            return $?
            ;;
        portage)
            portageq has_version "${ROOT}" "${1}" 2>/dev/null
            return $?
            ;;
        *)
            die "Unknown package manager: \"${manager}\"!"
    esac
}

get_repositories() {
    local manager=$(package-manager)
    case ${manager} in
        cave)
            $(cave_command) print-repositories
            ;;
        portage)
            for i in $(portageq envvar PORTDIR PORTDIR_OVERLAY); do
                portage_get_repo_name $i
            done
            ;;
        *)
            die "Unknown package manager: \"${manager}\"!"
    esac
}

get_news_dir_name() {
    [[ ${#@} -eq 1 ]] || die "get_news_dir_name expects exactly 1 argument!"

    local name=${1%::*} repo=${1##*::}
    local manager=$(package-manager)
    case ${manager} in
        cave)
            echo "$($(cave_command) print-repository-metadata --raw-name newsdir --format '%v\n' ${repo})/${name}"
            ;;
        portage)
            echo "$(portage_get_repository ${repo})/metadata/news/${name}"
            ;;
        *)
            die "Unknown package manager: \"${manager}\"!"
    esac
}

get_config_protect_dirs() {
    local manager=$(package-manager)
    case ${manager} in
        cave)
            cat "$($(cave_command) print-repository-metadata --raw-name location --format '%v\n' installed)"/.cache/all_CONFIG_PROTECT
            ;;
        portage)
            envvar sys-devel/gcc CONFIG_PROTECT
            ;;
        *)
            die "Unknown package manager: \"${manager}\"!"
    esac
}

# package-manager PRIVATE
# Return the package manager we're going to use.
package-manager() {
    local manager

    if [[ -n ${ECLECTIC_PACKAGE_MANAGER_CACHE} ]] ; then
        echo ${ECLECTIC_PACKAGE_MANAGER_CACHE}
        return
    fi

    # We prefer cave over portage
    if $(cave_command) --version > /dev/null 2>&1 ; then
        manager="cave"
    else
        manager="portage"
    fi

    echo ${manager}
    export ECLECTIC_PACKAGE_MANAGER_CACHE=${manager}
}
# vim: set ft=eclectic sw=4 sts=4 ts=4 et tw=80 :
