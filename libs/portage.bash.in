#!/usr/bin/env bash

# Copyright (c) 2005, 2007 Gentoo Foundation.
# This file is part of the 'eclectic' tools framework.
#
# eclectic is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# eclectic is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# eclectic; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA

# portageq ...
# Run portageq with safe filename as set by configure
portageq() {
    @PORTAGEQ@ "$@" 2> /dev/null
}

# portage_get_repo_name
# Return the name of the repository residing at $1
portage_get_repo_name() {
    [[ ${#@} -eq 1 ]] || die "portage_get_repo_name expects exactly 1 arguments!"
    if [[ -r ${1}/profiles/repo_name ]]; then
        cat ${1}/profiles/repo_name
    else
        basename ${1}
    fi
}

# portage_get_repository
# Return the path of the repository with the name given in $1
portage_get_repository()
{
    [[ ${#@} -eq 1 ]] || die "portage_get_repository expects exactly 1 arguments!"
    for i in $(portageq envvar PORTDIR PORTDIR_OVERLAY); do
        if [[ $(portage_get_repo_name ${i}) == ${1} ]]; then
            echo ${i}
            return
        fi
    done
}

# vim: set ft=eclectic sw=4 sts=4 ts=4 et tw=80 :
