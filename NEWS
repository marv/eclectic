This file contains a summary of changes in released versions.

2.0.21:
    Bug fixes:
    - alternatives: Provider names with dashes in them
      (ex. `pkg-config`) are now escaped when running
      `eclectic <alternative> script <provider>`
    New features:
    - config: The program for merging files can now be specified
      with the ECLECTIC_DIFF environment variable.
    - config: Old files can now be deleted when merging files
2.0.20:
    Bug fixes:
    - Silence errors when PATH contains non-existant paths
    - Don't put internal variables in the user's environment
    - Let eclectic alternative script export the selected provider
    New features:
    - Add accept-all-force command to config module
2.0.19:
    Bug fixes:
    - Don't hardcode (g)readlink if detected at configure time
    - modules/env: Create ld-${target}.path if it does not exist, and should
    - modules/env: Print only relevant file names when generating ldcache
    Other:
    - Simplify some coding and use less abstractions (canonicalize(),
      realpath vs. readlink)
2.0.18:
    New features:
    - Create separate ld-${CHOST}.path files for musl's library search paths.
2.0.17:
    Bug fixes:
    - Fix another stderr leak from man when running eclectic <alternative>
      script.
    - env module no longer tries to run ldconfig, on systems which do not have
      an ldconfig program. (non-glibc systems)
    - Some old legacy from Gentoo has been removed. (no more /var/lib/gentoo/news)
    New features:
    - eclectic config interactive now exits when given an EOF.
2.0.16:
    Bug fixes:
    - Default MANPATH is provided as fallback. This fix is important for man
      implementations that dislike -C.
    - Removed reference to non-existent kernel module from man page.
    New features:
    - eclectic env update now also generates /etc/environment.
    - PKG_CONFIG_PATH is now supported in eclectic ... script.
    Removed modules:
    - rc.eclectic was removed since baselayout is dead on Exherbo.
2.0.15:
    Bug fixes:
    - Alternatives script fixes for cross.
    - Support non-GNU rm/mv in eclectic config.
2.0.14:
2.0.13:
    New features:
    - Alternatives light support - alternatives modules that always use the best
      version and thus cannot be switched manually.
2.0.12:
2.0.11:
    New features:
    - fontconfig.eclectic has been rewritten to be simpler and work with
      fontconfig 2.10
    - new vim syntax highlighting support
2.0.10:
    Bug fixes:
    - eclectic news read and unread completions now properly list the -<index>
      options with their titles as descriptions.
    New features:
    - eclectic news read "1" now suggests eclectic news read -1.
2.0.9:
    New features:
    - eclectic news list now sorts by name. Not by repository. Since news items
      are prefixed with a date that means most recent first.
2.0.8:
    Bug fixes:
    - eclectic alternative set 1 now selects an alternative named "1". To select
      the alternative that has index 1 use eclectic alternative set -1.

    New features:
    - eclectic news list now enumerates news items. eclectic news read -<index>
      is now supported.
    - eclectic config now supports ranges. E.g. eclectic config display 1-3.
2.0.7:
    Removed modules:
    - bashcomp.eclectic has been removed as it is no longer needed. As of
      bash-completion 1.3_p20110628 completions for things that are installed
      are enabled by default.
2.0.6:
    Bug fixes:
    - best-version() was always broken and since it isn't used it has been
      removed.
    New features:
    - eclectic now prefers the cave client over the paludis client for querying
      the package manager.
    - The ECLECTIC_CONFIG_DIFF_COMMAND environment variable now lets you
      override the diff command for eclectic config.
2.0.5:
    New features:
    - Use git diff --color-words if present in eclectic config diff.
    - Support bash not being in /bin/.

2.0.4:
    Bug fixes:
    - Make eclectic env and eclectic rc handle quoting properly.
    - Make eclectic env rand eclectic rc espect ROOT properly.
    - Handle a missing config file that has config updates sensibly (bug #20).

    New features:
    - Add print-options action.
    - Let the alternatives, bashcomp, config, news and rc eclectic modules
      declare their options.
    - Use print-{modules,actions,options} in bash and zsh completions (bug #146).
    - List print-{modules,actions,options} in eclectic modules list.

    Removed modules:
    - kernel.eclectic has been removed as it is utterly pointless.

2.0.3:
    Bug fixes:
    - Make print-modules return 1 if no modules get listed.
    - list-modules would reinvoke eclectic without respecting options like
      --no-colour.
    - Various improved error handling for alternatives modules.

    New features:
    - Add files action to alternatives modules for listing symlinks provided by
      a currently selected provider.
    - Add group action to modules module for listing modules that belong to a
      specified group. The 'Built-in' and 'Extra' groups are special.
    - Add --group parameter for print-modules.
    - Add --debug parameter for eclectic.
    - Allow eclectic alternative script 1 to work.

2.0.2:
    Bug fix:
    - Alternatives would fail at overwriting symlinks that are pointing at a
      directory because it thought it wasn't a symlink.

2.0.1:
    New features:
    - Alternatives now supports 'set --force' and 'unset --force', allowing the
      user to overwrite or remove existing non-symlink files.

2.0.0:
    Bug fixes:
    - Various improvements in opengl module.

    New features:
    - Add a new module, 'modules', for listing, querying, and installing
      eclectic modules.
    - Allow setting system-wide default dictionary.
    - Added a new library for the Exherbo alternatives framework. Many old
      modules have been replaced by this.
    - Renamed to eclectic.
    - Improved shell completion support.
    - Lots of internal restructuring.

1.0.11:
    Bug fixes:
    - Allow resetting to the current implementation, so changes to the symlink
      map can propagate (bug #189942).

    New features:
    - Add stuff to be used by new package manager independent eselect-news.

1.0.10:
    Bug fixes:
    - Fix load_config to ignore influence of environment variables.

    New features:
    - Add noldconfig option.

1.0.9:
    Bug fixes:
    - Really make the makefiles parallel-safe.
    - Fixed bug #163915: eselect doesn't fully honour DESTDIR
    - Fix parsing of *_SEPERATED in env.eselect

1.0.8:
    Bug fixes:
    - Fixed bug #162008: make install isn't parallel-safe.

1.0.7:
    Bug fixes:
    - Fixed bug in modules/opengl.eselect: Fix when ROOT != /.
    - Fixed bug #151701: Generate {SPACE,COLON}_SEPERATED vars in update.
    - Fixed bug #152318: Fix env.eselect for envvar values containing '='.
    - Fixed bug #152662: Fixed new die() function on *BSD.
    - Updated developer documentation.

    Removed modules:
    - java.eselect is now superseded by java-{vm,nsplugin}.eselect.

1.0.6:
    Bug fixes:
    - Fixed bug #144152: Added reload action to rc module.
    - Fixed bug #140633: Fixed quoting in libs/output.bash.in
    - Fixed bug #144882: Corrected copy'n'paste errors
    - Fixed bug #147930: Die on un-sourceable files when handling config files.
    - Fixed bug #148534: inherit package-manager in profiles module.
    - Fixed bug #149627: Die on stray binutils targets.
    - Fixed bug #147857: Don't install vi module as part of eselect.

    New Features:
    - Enhance die function to work in deep subshells.

1.0.5:
    Bug fixes:
    - Fixed bug in libs/skel.bash.in regarding set action with numerical parameters.
    - Fixed bug #141106.

1.0.4:
    Bug fixes:
    - Fixed list bug in libs/skel.bash.in.

    New Features:
    - eselect is now independent of the used package-manager.

1.0.3:
    Bug fixes:
    - Fix env.eselect by white-listing allowed mime-types for files in
    /etc/env.d/.
    - Fix env.eselect to create proper /etc/profile.csh.

    New features:
    - blas.eselect and lapack.eselect have been rewritten, cblas.eselect has
    been split out of blas.eselect. All three modules now use skel.bash
    library.
    - rc.eselect now supports a non-default service directory.
    - Add oodict.eselect as a separately-shipped module.
    - kernel.eselect now understands target $(uname -r).

1.0.2:
    Bug fixes:
    - Fix env.eselect to not bail on missing /etc/profile.env.

    New features:
    - kernel.eselect now understands 'set $(uname -r)'.

1.0.1:
    Bug fixes:
    - Fix creation of /etc/profile.env in env.eselect.
    - Fix portability problems on *BSD in env.eselect.
    - Synchronized man pages with modules.

    New features:
    - binutils.eselect now features a show action.
    - Added arch-independent function to canonicalise paths.
    - eselect now supports *BSD.

1.0:
    Bug fixes:
    - Make no-color behave.
    - Fixed die filename reporting.
    - Restrict load_config to textfiles with proper contents.
    - Scan ld.so.conf rather than hardcoding libdirs.
    - Fix blas, lapack to work even if scan hasn't been called.

1.0_rc2:
    Bug fixes:
    - Fixed location of config files for blas.eselect and lapack.eselect.
    - Fixed behaviour of append_config to not add duplicate items.

    New features:
    - blas.eselect and lapack.eselect now fully support MKL72 (yet to be committed
    to portage tree).

1.0_rc1:
    Bug fixes:
    - Added missing description of --global flag in the bashcomp module's
      manual page (bug #101898).
    - kernel module's 'list' action now only lists kernels and not everything
      in /usr/src (bug #104354).
    - Fixed unexpected EOF error when calling the rc module's 'restart'
      action (bug #106540).
    - Fixed a 'file not found' bug in the rc module when encountering bogus
      baselayouts (thanks to Mike Doty <kingtaco@gentoo.org>).
    - Fixed bug in usage of has() library function in main eselect script
      (thanks to Sven Wegener <swegener@gentoo.org>).
    - Fixed unportable uses of absolute paths to binaries.
    - Exit with an error if the user doesn't provide parameters to the various
      rc module actions (start, stop, restart, etc).
    - Our autogen script now works on non-linux boxes.
    - Added symlinks for the manual pages of the modules we already provide
      symlinks for.
    - Added missing manual page for kernel module.

    New features:
     - inherit() support that allows dynamic and individual sourcing of
       libraries for each module.
     - Added support for modules to be able to describe the options/parameters
       they accept via describe_ACTION_options() and
       describe_ACTION_parameters().  These descriptions now show up in the
       help/usage output for the respective module.
     - profile module's 'set' action now accepts a --force flag for forcing the
       setting of a profile.
     - smart line wrapping for lines longer than the current terminal width.

0.9.6:
    Bug Fixes:
    - Fixed crippled output of items that contain spaces in write_list
    functions.
    - Fixed store_config() to also store empty values.

    New Features:
    - Moved both binutils and env module out of dodgy-scripts.
    - Added env module to replace env-update.
    - Added dirname()/basename() function in pure bash to reduce number of
      calls to external binaries.

0.9.5:
    Bug Fixes:
    - Fixed nocolour handling that caused literal interpretation of '*' thus
      causing the contents of ${PWD} to be displayed instead.
    - Now recognizes the us'ian --no-color in addition to --no-colour.
    - Reverted the colours implementation to the previously used one as all
      the 'colours' calls in a sub-shell made displaying things quite slow.
    - Removed hardcoded paths to binaries that were present in certain modules.
    - Added GNU sed checks so we use the proper path thus allowing it to work
      on systems whose GNU sed is 'gsed'.
    - bashcomp module now properly supports using corresponding numbers
      (from list action) when running enable/disable action.
    - eselect bash-completion now recognizes (and completes on) global options.

0.9.4:
    Bug Fixes:
    - fixed call to lapack config file in blas module.

    New Features:
    - added a testing version of binutils.eselect.
    - added (start|stop|restart) subactions to rc module.
    - implemented global options handling generally and a --no-colour
    option specifically.
    - all modules mark currently active options with a * in list subaction.

0.9.3:
    Bug Fixes:
    - mailer and kernel modules: handle no targets better, show current
    selection when doing a list.
    - usage message should now always be correct.
    - support for many more symlink prefixes and suffixes.

    New Features:
    - more documentation.
    - rc module as a replacement for rc-config and rc-status.

0.9.2:
    Bug Fixes:
    - blas/lapack modules now use 'scan' subcommand over 'update'.
    - profile module now works with current profiles.desc format.
    - mailer module now works (previously broken due to typo and
      missing test in find_targets glob loop).
    - bashcomp module's show sub-command no longer returns 1.

/* vim: set sw=4 et sts=4 tw=80 spell spelllang=en : */
